var zfighters
var soldiers
var warriors
obj/spawnPoint
	density = 0
	var team
	var map
obj/selection
	icon = 'Selection.dmi'
	var Z
	Click()
		usr.loc = locate(22, 8, Z)
	zfighter
		icon_state = "zfighter"
		Z = 2
	soldier
		icon_state = "soldier"
		Z = 3
	soldiere
		icon = 'SoldierExtra.dmi'
		Z = 3
	warrior
		icon_state = "warrior"
		Z = 4
	back
		icon = 'SelectionBack.dmi'
		Z = 1
obj/character
	var selected
	var team
	var list/hotbar = list()
	var list/passives = list()
	var maxHealth = 175, maxEnergy = 200, maxStamina = 150
	var healthRegen = 0.04, energyRegen, staminaRegen = 0.1
	var attack = 10, attackRange = 16, attackDelay = 3
	var trip = 4, knock = 16
	var icon/sIcon
	var powerup = "powerup"
	Click()
		if(selected || !canSelect())
			return
		else
			selected = 1
			var/icon/I = icon('Avatars.dmi', "SELECT")
			overlays.Add(I)
			switch(team)
				if("zfighters")
					zfighters++
				if("soldiers")
					soldiers++
				if("warriors")
					warriors++
			usr.team = team
			usr.icon = sIcon
			usr.charName = name
			usr.characterStat(src, 1)
			usr.characterSkills(src)
			usr.mapSet()
			winshow(usr, "health", 1)
			winshow(usr, "energy", 1)
			winshow(usr, "stamina", 1)
			moveLoop.Add(usr)
	proc
		canSelect()
			if(team == "zfighters" && zfighters > soldiers)
				alert("The Z-Fighters can not outnumber the Soldiers.")
				return FALSE
			if(team == "soldiers" && soldiers > zfighters)
				alert("The Soldiers can not outnumber the Z-Fighters.")
				return FALSE
			if(team == "warriors" && (warriors > soldiers || warriors > zfighters))
				alert("The Warriors can not outnumber the Soldiers or the Z-Fighters.")
				return FALSE
			return TRUE
	icon = 'Avatars.dmi'
	zfighters
		team = "zfighters"
		goku
			maxHealth = 200
			healthRegen = 0.05
			maxStamina = 200
			name = "Goku"
			icon_state = "GOKU"
			sIcon = 'Goku.dmi'
			New()
				hotbar = list(global.skills["kamehameha"], global.skills["energyblast"], global.skills["kaiokenrush"], global.skills["spiritbomb"], global.skills["instanttransmission"], global.skills["superkamehameha"])
		trunks
			name = "Trunks"
			icon_state = "TRUNKS"
			sIcon = 'Trunks.dmi'
			New()
				hotbar = list(global.skills["galickgun"], global.skills["masenko"], global.skills["heal"], global.skills["evilconwave"], global.skills["godbreaker"])
		vegeta
			sIcon = 'Vegeta.dmi'
			name = "Vegeta"
			icon_state = "VEGETA"
			maxHealth = 200
			maxStamina = 200
			attack = 11.5
			attackDelay = 3.25
			New()
				hotbar = list(global.skills["lucoragun"], global.skills["amazingimpact"], global.skills["finalburstcannon"], global.skills["explosivewave"], global.skills["galickgun"])
	soldiers
		team = "soldiers"
		cooler
			powerup = "coolerpower"
			name = "Cooler"
			sIcon = 'Cooler.dmi'
			icon_state = "COOLER"
			maxEnergy = 275; maxStamina = 180; maxHealth = 160
			New()
				hotbar = list(global.skills["darknessbeam"], global.skills["superdeathbeam"], global.skills["sauzerblade"], global.skills["psychobarrier"], global.skills["coolersupernova"])
		freeza
			attackDelay = 2
			attack = 9
			maxEnergy = 250
			name = "Freeza"
			icon_state = "FREEZA"
			sIcon = 'Freeza.dmi'
			New()
				hotbar = list(global.skills["deathbeam"], global.skills["deathbullet"], global.skills["energywave"], global.skills["conqwrath"], global.skills["supernova"])
		frost
			sIcon = 'Frost.dmi'
			attackDelay = 2.5
			attack = 9.25
			icon_state = "FROST"
			name = "Frost"
			New()
				hotbar = list(global.skills["chaosbeam"], global.skills["deathcannon"], global.skills["phantomfist"], global.skills["assaultdeathbullet"], global.skills["chaosball"])
	warriors
		team = "warriors"
		cell
			attackRange = 24
			maxHealth = 250; maxEnergy = 225; maxStamina = 300; attackDelay = 4; healthRegen = 0.06
			name = "Cell"
			icon_state = "CELL"
			sIcon = 'CellSemi.dmi'
			New()
				hotbar = list(global.skills["cellgalickgun"], global.skills["bigbangcrash"], global.skills["solarflare"], global.skills["unforgivable"], global.skills["bioabsorb"])
			Click()
				..()
				usr.deathPassives.Add(new /skill/bioRegeneration)
		black
			sIcon = 'Black.dmi'
			name = "Black"
			icon_state = "BLACK"
			maxHealth = 200; maxStamina = 200
			New()
				hotbar = list(global.skills["blackkamehameha"], global.skills["blackbullet"], global.skills["godslicer"], global.skills["blacktransmission"], global.skills["timedistortion"])
		broly
			sIcon = 'BrolyRssj.dmi'
			name = "Broly"
			icon_state = "BROLY"
			maxHealth = 250; maxStamina = 300
			New()
				hotbar = list(global.skills["bloodysmash"], global.skills["eraserblow"], global.skills["erasershot"], global.skills["blastermeteor"], global.skills["erasercannon"])
mob
	proc
		characterStat(obj/character/O, select)
			hotbar = O.hotbar
			maxHealth = O.maxHealth
			baseHealth = O.maxHealth
			maxEnergy = O.maxEnergy
			baseEnergy = O.maxEnergy
			maxStamina = O.maxStamina
			baseStamina = O.maxStamina
			healthRegen = O.healthRegen
			baseHR = O.healthRegen
			energyRegen = O.energyRegen
			baseER = O.energyRegen
			staminaRegen = O.staminaRegen
			baseSR = O.staminaRegen
			attack = O.attack
			baseAttack = O.attack
			attackDelay = O.attackDelay
			attackRange = O.attackRange
			tripAmount = O.trip
			knockAmount = O.knock
			if(select)
				maxStats()
				var/buff/B = new /buff
				B.id = "default"
				buffs.Add(B)

		characterSkills(obj/character/O)
			hotbar = O.hotbar
			passives = list(global.skills["[O.powerup]"])
		mapSet()
			for(var/obj/spawnPoint/P in world)
				if(P.map == map && P.team == team)
					SetCenter(P)