buff
	var id
	var health
	var energy
	var stamina
	var hR
	var eR
	var sR
	var attack
mob/proc/buffStats(bA, bH, bE, bS, bHR, bER, bSR)
	for(var/buff/B in effects)
		bA += B.attack
		bH += B.health
		bE += B.energy
		bS += B.stamina
		bHR += B.hR
		bER += B.eR
		bSR += B.sR
	attack = clamp(baseAttack + bA, 0, baseAttack * 2)
	maxHealth = clamp(baseHealth + bH, 30, baseHealth * 2)
	maxEnergy = clamp(baseEnergy + bE, 50, baseEnergy * 2)
	healthRegen = clamp(baseHR + bHR, baseHR * 0.25, baseHR * 5)
	energyRegen = clamp(baseER + bER, baseER * 0.1, baseER * 2.5)
	staminaRegen = clamp(baseSR + bSR, baseSR * 0.1, baseSR * 2.5)
	maxStamina = clamp(baseStamina + bS, 80, baseStamina * 2)
	healthChange()
	energyChange()
	staminaChange()
