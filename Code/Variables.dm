mob
	dir = EAST
	Stat()
		if(statpanel("Skills") && (passives.len || hotbar.len))
			var/skill/S
			for(var/C in 1 to passives.len)
				S = passives[C]
				if(!S.hidden)
					if(S.descOnly && cooldowns[S.cooldownID] > world.time)
						stat("<font color = red>[S.desc]</font>")
					else if(S.descOnly)
						stat("<font color = #FFA500>[S.desc]</font>")
					else if(S.desc && cooldowns[S.cooldownID] > world.time)
						stat("<font color = red>[skillKeys[C + 10]]</font>", "<font color = red>[S.desc]</font>")
					else if(S.desc)
						stat("[skillKeys[C + 10]]", "<font color = #FFA500>[S.desc]</font>")
			for(var/C in 1 to hotbar.len)
				S = hotbar[C]
				if(S.cooldownTime && cooldowns[S.cooldownID] > world.time)
					stat("<font color = red>[skillKeys[C]]</font>", "<font color = red>[S.name] - Cooldown</font>")
				else
					stat("[skillKeys[C]]", "<font color = white>[S.name]</font>")
				if(descs)
					stat("<font color = yellow>[S.desc]</font>")
	var
		dead = 0
		team
		charName

		list/buffs = list()
		list/binds = list()
		list/effects = list()
		list/cooldowns = list()
		list/hotbar = list()
		list/passives = list()
		list/loopPassives = list()
		list/hitPassives = list() // Passives when you get hit.
		list/hitOtherPassive = list() // Passives when you hit others.
		list/deathPassives = list() // Passives called when you die.
		list/killPassives = list() // Passives called when you kill someone.
		list/skillKeys = list("1", "2", "3", "4", "5", "Q", "W", "E", "R", "T", "A", "S", "D")
		castingSkill
		castingID

		health
		maxHealth
		energy
		maxEnergy
		stamina
		maxStamina
		doesRevive = 1
		icon/oldIcon

		baseHealth
		baseEnergy
		baseStamina
		baseHR
		baseER
		baseSR
		baseAttack

		healthRegen
		energyRegen
		staminaRegen

		attack
		attackRange
		attackDelay

		trip
		knock
		tripAmount
		knockAmount
world
	view = "44x17"