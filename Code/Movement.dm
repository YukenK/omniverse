client
	Northeast()
		return
	Northwest()
		return
	North()
		return
	South()
		return
	Southwest()
		return
	Southeast()
		return
	West()
		return
	East()
		return
var list/moveLoop = list()

proc
	movementLoop()
		for()
			for(var/mob/M in moveLoop)
				M.moveLoop()
			sleep(world.tick_lag)

atom/movable
	proc
		bottom(px)
			return obounds(src, 0, -px)
		top(px)
			return obounds(src, 0, px)
		front(px)
			var offsetX, extraWidth
			extraWidth = px - bound_width
			if(dir & EAST)
				offsetX = bound_width
			else
				offsetX = -px
			return obounds(src, offsetX, 0, extraWidth, 0)
		frontDir(mob/M)
			if(dir == EAST && M.Cx() > Cx() || dir == WEST && M.Cx() < Cx())
				return TRUE
		behindDir(mob/M)
			if(dir == EAST && M.Cx() < Cx() || dir == WEST && M.Cx() > Cx())
				return TRUE
		behind(px)
			return obounds(src, Directions.ToOffsetX(dir) * -px, Directions.ToOffsetY(dir) * -px, 0, 0)
		area(px)
			return obounds(src, bound_width / 2 - px, bound_height / 2 - px, px * 2, px * 2)
mob
	density = 1
	Cross()
		if(dead)
			return TRUE
		. = ..()
	Login()
		if(!charName)
			loc = locate(22, 8, 1)
	var velX
	var velY // Only modified by player input.
	var skillVelX
	var skillVelY // Velocity modified by skills.
	var onGround = 0
	var gravity = 5
	var gravRate = 1
	var nextJump = 0
	var moveX = 0
	var list/keys = list()

	var moveSpeed = 2
	var maxMoveSpeed = 10
	var runSpeed = 4
	var maxRunSpeed = 16
	var isRun = 1
	var decelRate = 1
	var jumpSpeed = 20
	var canFall = 1
	verb
		keyDown(k as text)
			set hidden = 1
			keys.Add(k)
			if(k in skillKeys)
				hotKey(skillKeys.Find(k))
			if(k == "Z")
				Jump()
			if(k == "North")
				knock = 1
			if(k == "South")
				trip = 1
			if(k == "C")
				Attack()
		keyUp(k as text)
			set hidden = 1
			keys.Remove(k)
			if(k == "North")
				knock = 0
			if(k == "South")
				trip = 0
	proc
		mobMove(pVelX, pVelY)
			if(canMove())
				Translate(pVelX + skillVelX, 0)
			else
				Translate(skillVelX, 0)
			if(canFall())
				Translate(0, (skillVelY + pVelY) - gravity)
		moveLoop()
			Regeneration()
			getInput()
			bindRemove()
			effectRemove()
			passiveSkills()
			Acceleration()
			Gravity()
			mobMove(velX, velY)
			setState()
		Gravity()
			if(velY > 0)
				velY = clamp(velY - gravRate, 0, 300)
			else if(velY < 0)
				velY = clamp(velY + gravRate, -300, 0)
			if(skillVelY > 0)
				skillVelY = clamp(skillVelY - gravRate, 0, 300)
			else if(skillVelY < 0)
				skillVelY = clamp(skillVelY + gravRate, -300, 0)
			if(!onGround)
				nextJump = world.time + 1
			for(var/turf/T in obounds(src, 0, -1))
				if(T.density)
					onGround = 1
					break
				else
					onGround = 0
		getInput()
			if(keys.Find("East") && keys.Find("West"))
				moveX = 0
			else if(keys.Find("East"))
				moveX = 1
				if(canTurn())
					dir = EAST
			else if(keys.Find("West"))
				moveX = -1
				if(canTurn())
					dir = WEST
			else
				moveX = 0
		Acceleration()
			if(moveX != 0 && canMove())
				if(isRun && canRun())
					velX = clamp(velX + runSpeed * moveX, -maxRunSpeed, maxRunSpeed)
				else
					velX = clamp(velX + moveSpeed * moveX, -maxMoveSpeed, maxMoveSpeed)
			else
				if(velX > 0)
					velX = clamp(velX - decelRate, 0, maxRunSpeed)
				else
					velX = clamp(velX + decelRate, -maxRunSpeed, 0)
			if(skillVelX > 0)
				skillVelX = clamp(skillVelX - decelRate, 0, 300)
			else if(skillVelX < 0)
				skillVelX = clamp(skillVelX + decelRate, -300, 0)
		canTurn()
			if(binds.len)
				for(var/bind/B in binds)
					if(B.turn)
						return TRUE
				return FALSE
			else
				return TRUE
		canRun()
			return canMove()
		canMove()
			if(binds.len)
				return FALSE
			return !dead
		canFall() // If you can fall out of the air.
			return canFall
		canJump()
			if(stamina < 10 || !onGround || world.time < nextJump)
				return FALSE
			return canMove()
		Jump()
			if(canJump())
				staminaChange(-10)
				velY = jumpSpeed
		setState()
			if(dead)
				icon_state = "DEAD"
			else if(bindState())
				for(var/bind/iconState/B in binds)
					icon_state = B.iconState
			else if(!onGround)
				icon_state = "JUMP"
			else if(velX != 0)
				if(isRun && canRun())
					icon_state = "RUN"
				else
					icon_state = "MOVE"
			else
				icon_state = "STAND"
		bindState()
			for(var/bind/iconState/B in binds)
				return TRUE