var
	skillManager/skillManager = new()
	list/skills = list()
proc/getType(type)
	if(ismob(type))
		return type
	if(istype(type, /beam))
		var/beam/B = type
		return B.owner
	if(istype(type, /blast))
		var/blast/B = type
		return B.owner
	if(istype(type, /dash))
		var/dash/D = type
		return D.owner
mob
	proc
		canChargeMove(skill/S)
			for(var/bind/B in binds)
				if(B.id != S.chargeID)
					return FALSE
			return TRUE
		hotKey(key)
			var/skill/S
			if(key > 10)
				var X = key
				X -= 10
				if(passives[X])
					S = passives[X]
			else if(hotbar.len >= key)
				if(hotbar[key])
					S = hotbar[key]
			if(S)
				S.Use(src, skillKeys[key])
		passiveSkills()
			for(var/skill/S in loopPassives)
				S.Used(src)

skillManager
	New()
		var/skill/s, sid
		for(var/v in typesof(/skill))
			s = v
			if(sid = initial(s.id))
				global.skills[sid] = new v()
skill
	var id
	var cooldownID
	var cooldownTime
	var doesCD = 1
	var desc
	var name
	var descOnly
	var hidden
	var passiveUse // Lower number = gets called first.

	var castTime // How long it takes to cast.
	var skillTime // How long it binds you after casting.
	var castRemove = 1
	var skillRemove = 1 // If the bind will be removed with damage.

	var energyCost
	var staminaCost
	var doesCharge
	var doesChargeCD // If charging the skill will put it on cooldown.
	var endCD = 1 // Used for power-up and similar skills, puts it on cooldown when you finish.
	var onlyCharge // If the skill is only usable when it has been fully charged.
	var damageCharge = 1 // If taking damage will end a charge early.
	var chargeState
	var chargeTime
	var minChargeTime
	var minChargeState
	var chargeDrain
	var chargeID
	var chargePercentage // If true, the skill can have a variable charge percentage. Otherwise, it will use either the max or least charge.
	var damageEnd

	var castState
	var skillState
	var castFlick
	var skillFlick
	var chargeTurn

	proc/canUse(mob/user, charging)
		if(user.energy < energyCost || user.stamina < staminaCost || user.cooldowns[cooldownID] > world.time || !(user in moveLoop))
			return FALSE
		if(!charging)
			if(!user.canMove())
				return FALSE
		else
			if(!user.canChargeMove(src))
				return FALSE
		return TRUE
	proc/Use(mob/user, key)
		if(canUse(user) && !doesCharge)
			Used(user)
		else if(canUse(user, 1) && doesCharge)
			var startTime = world.time
			var endTime = world.time + chargeTime
			var minCharge
			var charged
			var health = user.health
			user.castingSkill = 1
			user.castingID = id
			var/bind/iconState/B = new /bind/iconState(_time = chargeTime, _damageRemove = damageCharge, _iconState = chargeState, _id = chargeID, _turn = chargeTurn)
			if(chargeTime == -1)
				B.time = -1
			user.binds.Add(B)
			while((key in user.keys) && (B in user.binds) && canUse(user, 1))
				if(!(user in moveLoop))
					return
				if(minChargeTime && world.time >= startTime + minChargeTime)
					B.iconState = minChargeState
					minCharge = 1
				if(chargeDrain)
					user.energyChange(-chargeDrain)
				if(minChargeTime && !minCharge)
					goto END
				if(world.time >= endTime || chargeTime == -1)
					Used(user, 1)
					charged = 1
					if(chargeTime != -1)
						break
				END
				sleep(world.tick_lag)
			user.binds.Remove(B)
			if(endCD)
				user.cooldowns[cooldownID] = world.time + cooldownTime
			if(!onlyCharge && chargeTime != -1 && !charged)
				if(minChargeTime && world.time < startTime + minChargeTime || damageEnd && user.health < health)
					return
				Used(user, (world.time - startTime) / (endTime - startTime))
	proc/Used(mob/user, charged)
		if(cooldownTime && cooldownID && doesCD)
			user.cooldowns[cooldownID] = world.time + cooldownTime
		if(energyCost)
			user.energyChange(-energyCost)
		if(staminaCost)
			user.staminaChange(-staminaCost)
		if(castTime)
			user.castingSkill = 1
			user.castingID = id
			var endTime = world.time + castTime
			if(castFlick)
				flick(castFlick, user)
			var/bind/B = new /bind/iconState(_time = castTime + 10, _iconState = castState, _damageRemove = castRemove, _turn = chargeTurn)
			user.binds.Add(B)
			while(B in user.binds)
				if(!(user in moveLoop))
					return
				if(world.time >= endTime)
					user.binds.Remove(B)
				sleep(world.tick_lag)
			if(world.time < endTime)
				return
		if(skillTime)
			if(skillFlick)
				flick(skillFlick, user)
			if(skillState)
				user.binds.Add(new /bind/iconState(_time = skillTime, _iconState = skillState, _damageRemove = skillRemove))
			else
				user.binds.Add(new /bind(_time = skillTime, _damageRemove = skillRemove))
