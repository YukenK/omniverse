mob
	proc
		canRegen(type)
			if((type == "STAMINA") && isRun && canRun() && moveX != 0)
				return FALSE
			return !dead
		Regeneration()
			if(canRegen("HEALTH"))
				healthChange(healthRegen)
			if(canRegen("STAMINA"))
				staminaChange(staminaRegen)
			statBars()
		canDamage()
			for(var/effect/shield/S in effects)
				return FALSE
			return TRUE
		healthChange(i, _deathCheck = 1, _bindRemove = 1, _minHealth = 0, _hitter)
			if(canDamage())
				health = clamp(health + i, _minHealth, maxHealth)
			if(i < 0 && _bindRemove)
				for(var/bind/B in binds)
					if(B.damageRemove)
						binds.Remove(B)
			if(_hitter)
				for(var/skill/S in hitPassives)
					S.Used(src, _hitter)
			if(!dead && _deathCheck)
				deathCheck()
		energyChange(i)
			energy = clamp(energy + i, 0, maxEnergy)
		staminaChange(i)
			stamina = clamp(stamina + i, 0, maxStamina)
		maxStats()
			health = maxHealth
			energy = maxEnergy
			stamina = maxStamina
		statBars()
			if(client)
				winset(src, "default.health", "value = [round(health/maxHealth * 100)]")
				winset(src, "default.stamina", "value = [round(stamina/maxStamina * 100)]")
				winset(src, "default.energy", "value = [round(energy/maxEnergy * 100)]")