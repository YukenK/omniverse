skill
	cellGalickGun
		id = "cellgalickgun"
		cooldownID = "galickgun"
		castTime = 3
		castState = "CHARGE"
		energyCost = 50
		name = "Galick Gun"
		desc = "Quick-fire beam, deals 50 damage."
		cooldownTime = 150
		Used(mob/user)
			..()
			var/beam/B = new /beam(user, 'galickGun.dmi', 20, 15, 50, _castSkill = src, _name = "Galick Gun")
			if(B)
				B.H.pixel_y = -32
				if(B.dir == EAST)
					B.T.icon = null
					B.B.pixel_x = 32
	bigBangCrash
		id = "bigbangcrash"
		cooldownID = "bigbang"
		castTime = 3
		castState = "CHARGE"
		energyCost = 70
		name = "Big Bang Crash"
		desc = "Quick-fire blast, deals 40 damage & explodes."
		cooldownTime = 250
		Used(mob/user)
			..()
			new /blast(user, _icon = 'bigBangCrash.dmi', _moveSpeed = 14, _damage = 40, _tier = 2, _castSkill = src, _name = "Big Bang Crash", _time = 25, _skill = new /skill/bigBangCrashPassive)
	solarflare
		id = "solarflare"
		cooldownID = "solarflare"
		energyCost = 30
		name = "Solar Flare"
		desc = "Instantly stun all nearby enemies."
		cooldownTime = 250
		skillFlick = "BLAST"
		skillTime = 4
		Used(mob/user)
			..()
			for(var/mob/M in view(user))
				if(user.canHit(M))
					M.binds.Add(new /bind/iconState(_time = 7, _iconState = "STUN"))
	unforgivable
		id = "unforgivable"
		cooldownID = "unforgivable"
		energyCost = 40
		castTime = 16
		castState = "BOMBCAST"
		name = "Unforgivable!"
		desc = "After a short delay, begin the cast. After 4 seconds, or if you get damaged, you will explode - dealing massive damage to nearby enemies and removing your body from the map. Regeneration can proc with Unforgivable."
		cooldownTime = 2400
		Used(mob/user)
			..()
			var/bind/B = new /bind/iconState(_time = 60, _iconState = "BLOW")
			var/textProc = 0
			user.binds.Add(B)
			flick("BOMB", user)
			var/time = world.time + 10
			while(time > world.time && (B in user.binds))
				textProc++
				if(textProc == 1)
					world << output("<font size = 4>[user.charName]: This... this is...",  "chat")
				else if(textProc == 20)
					world << output("<font size = 4>[user.charName]: UNFORGIVABLE!", "chat")
					textProc++
				sleep(world.tick_lag)
			time = world.time + 40
			var/lastHealth = user.health
			while(user.health >= lastHealth && time > world.time)
				if(!(user in moveLoop))
					return
				sleep(world.tick_lag)
			for(var/mob/M in view(user))
				if(user.canHit(M))
					M.healthChange(-300, _hitter = src)
			world << output("<font size = 4>[user.charName]: BOOM!", "chat")
			user.healthChange(-user.maxHealth)
			user.oldIcon = user.icon
			user.doesRevive = 0
			user.icon = null
	bioAbsorb
		id = "bioabsorb"
		cooldownID = "bioabsorb"
		energyCost = 100
		castTime = 80
		castFlick = "RUSH"
		skillTime = 5
		skillFlick = "RUSH2"
		name = "Bio-Absorb"
		desc = "After 8 seconds, absorb a single enemy body nearby, gaining 10% of their reserves."
		cooldownTime = 600
		cooldownID = "bioabsorb"
		Used(mob/user)
			..()
			for(var/mob/M in user.area(64))
				if(M.team != user.team && M.doesRevive)
					M.doesRevive = 0
					M.oldIcon = M.icon
					M.icon = null
					for(var/buff/B in user.buffs)
						if(B.id == "default")
							B.hR += M.baseHR * 0.1
							B.eR += M.baseER * 0.1
							B.sR += M.baseSR * 0.1
							B.health += M.baseHealth * 0.1
							B.energy += M.baseEnergy * 0.1
							B.stamina += M.baseStamina * 0.1
					break
			user.buffStats()

	bioRegeneration
		Used(mob/user)
			if(prob(25))
				spawn(50)
					if(!(user in moveLoop))
						return
					world << output("[user.charName] has regenerated!", "feed")
					user.health = user.maxHealth * 0.5
					user.energy = user.maxEnergy * 0.8
					user.stamina = user.maxStamina
					user.dead = 0
					user.doesRevive = 1
					switch(user.team)
						if("zfighters")
							zfighters++
						if("soldiers")
							soldiers++
						if("warriors")
							warriors++
					if(user.icon == null)
						user.icon = user.oldIcon
					flick("REGEN", user)
	bigBangCrashPassive
		Used(mob/target, mob/owner, beam/controller)
			spawn()
				new /explosion(target, 128, 128, _damage = 30, _icon = 'Explosion.dmi', _knockX = 16, _knockY = 24, _time = 5)
