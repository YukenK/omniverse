skill
	lucoraGun
		id = "lucoragun"
		cooldownID = "lucoragun"
		energyCost = 40
		name = "Lucora Gun"
		desc = "Rapidly fire blasts for 1.4 seconds, once every 0.2 seconds."
		cooldownTime = 450
		Used(mob/user)
			..()
			var/bind/B = new /bind/iconState(_time = 19, _iconState = "BLAST")
			var/time = 0
			user.binds.Add(B)
			while(B in user.binds)
				if(!(user in moveLoop))
					return
				if(world.time > time)
					time = world.time + 2
					spawn()
						new /blast(user, _icon = 'Blast.dmi', _damage = 8, _time = 15, _moveSpeed = 12, _castSkill = src, _name = "Death Bullet")
				sleep(world.tick_lag)
	amazingImpact
		id = "amazingimpact"
		name = "Amazing Impact"
		desc = "One-tile melee. Knock an enemy up into the air, back down, then knocking them back."
		cooldownID = "amazingimpact"
		cooldownTime = 300
		energyCost = 50
		skillTime = 16
		skillState = "RUSH"
		Used(mob/user)
			..()
			var/time = world.time - 1
			var HIT = 0
			for(var/mob/M in user.front(48))
				if(user.canHit(M))
					var/bind/B = new /bind(_time = 40, _binders = list(user, M))
					user.binds.Add(B)
					M.binds.Add(B)
					while((B in user.binds) && HIT < 3)
						if(!(user in moveLoop))
							return
						switch(HIT)
							if(0)
								user.skillVelY = 24
								M.skillVelY = 24
							if(1)
								user.skillVelY = -24
								M.skillVelY = -24
						if(world.time > time)
							switch(HIT)
								if(0)
									M.healthChange(-15, _bindRemove = 0)
								if(1)
									M.healthChange(-15, _bindRemove = 0)
								if(2)
									M.healthChange(-15, _bindRemove = 0)
									M.skillVelX += 32 * -Directions.ToOffsetX(M.dir)
							time = world.time + 5
							HIT++
						sleep(world.tick_lag)
					user.binds.Remove(B)
					M.binds.Remove(B)
					break
	finalBurstCannon
		id = "finalburstcannon"
		name = "Final Burst Cannon"
		desc = "Quick-fire beam, deals 35 damage & explodes."
		castState = "CHARGE"
		castTime = 4
		cooldownTime = 200
		cooldownID = "energywave"
		energyCost = 40
		Used(mob/user)
			..()
			new /beam(user, 'deathWave.dmi', 20, 12, 40, _castSkill = src, _name = "Final Burst Cannon", _skill = new /skill/energyWavePassive, _beamState = "BLAST2")
	galickGun
		id = "galickgun"
		cooldownID = "galickgun"
		castTime = 5
		castState = "CHARGE"
		energyCost = 50
		name = "Galick Gun"
		desc = "Quick-fire beam, deals 50 damage."
		cooldownTime = 150
		Used(mob/user)
			..()
			var/beam/B = new /beam(user, 'galickGun.dmi', 20, 15, 50, _castSkill = src, _name = "Galick Gun")
			B.H.pixel_y = -32
			if(B.dir == EAST)
				B.T.icon = null
				B.B.pixel_x = 32
	explosiveWave
		id = "explosivewave"
		cooldownID = "explosivewave"
		skillTime = 4
		skillFlick = "RANGE"
		cooldownTime = 200
		energyCost = 40
		name = "Explosive Wave"
		desc = "Knockback & stun all nearby enemies."
		Used(mob/user)
			..()
			for(var/mob/M in user.area(256))
				if(user.canHit(M))
					M.skillVelX += 32 * -Directions.ToOffsetX(M.dir)
					M.skillVelY += 24