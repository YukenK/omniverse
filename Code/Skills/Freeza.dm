skill
	deathBeam
		id = "deathbeam"
		name = "Death Beam"
		skillTime = 2
		skillState = "SKILL2"
		desc = "Quick-fire beam, deals 40 damage, passes through enemies."
		cooldownTime = 120
		energyCost = 50
		cooldownID = "deathbeam"
		Used(mob/user)
			..()
			new /beam(user, _icon = 'deathBeam.dmi', _damage = 40, _bindTime = 20, _dense = 0, _moveSpeed = 14, _castSkill = src, _name = "Death Beam", _tier = 1)
	deathBullet
		id = "deathbullet"
		name = "Death Bullet"
		skillTime = 2
		skillFlick = "BLAST"
		desc = "Quick-fire blast, deals 15 damage, knockbacks & stuns."
		cooldownTime = 10
		cooldownID = "deathbullet"
		energyCost = 25
		Used(mob/user)
			..()
			new /blast(user, _icon = 'deathBullet.dmi', _damage = 15, _skill = new /skill/deathBulletPassive, _time = 15, _moveSpeed = 12, _castSkill = src, _name = "Death Bullet")
	deathBulletPassive
		Used(mob/target, mob/owner, blast/controller)
			target.skillVelX += 16 * Directions.ToOffsetX(controller.dir)
			target.binds.Add(new /bind/iconState(_time = 2, _iconState = "STUN"))
	energyWave
		id = "energywave"
		name = "Energy Wave"
		desc = "Quick-fire beam, deals 30 damage & explodes."
		castState = "BEAM2"
		castTime = 4
		cooldownTime = 200
		cooldownID = "energywave"
		energyCost = 40
		Used(mob/user)
			..()
			new /beam(user, 'deathWave.dmi', 20, 12, 40, _castSkill = src, _name = "Energy Wave", _skill = new /skill/energyWavePassive, _beamState = "BEAM2")
	energyWavePassive
		Used(mob/target, mob/owner, beam/controller)
			new /explosion(target, 96, 96, _damage = 20, _icon = 'Explosion.dmi', _knockX = 16, _knockY = 24, _time = 5)
	conquerorsWrath
		id = "conqwrath"
		name = "Conqueror's Wrath"
		desc = "Instantly stun an opponent within one tile in front of you, dealing damage."
		cooldownTime = 90
		skillTime = 3
		skillState = "RUSH"
		energyCost = 40
		cooldownID = "conqwrath"
		Used(mob/user)
			..()
			user.binds.Add(new /bind(_time = user.attackDelay))
			for(var/mob/M in user.front(32))
				if(user.canHit(M))
					M.healthChange(-user.attack, _hitter = user)
					M.binds.Add(new /bind/iconState(_time = 15, _iconState = "STUN"))
	superNova
		id = "supernova"
		name = "Supernova"
		desc = "If uninterrupted for 3 seconds, summon a Supernova. Deals AOE damage and explodes."
		castTime = 30
		castState = "CHARGE"
		skillTime = 15
		skillState = "BOMB"
		energyCost = 100
		cooldownTime = 600
		cooldownID = "supernova"
		chargeTurn = 1
		Used(mob/user)
			..()
			new /blast(user, _icon = 'Supernova.dmi', _height = 96, _skill = new /skill/superNovaPassive, _time = 40, _moveSpeed = 10, _skillUse = 1, _castSkill = src, _name = "Supernova", _tier = 3)
	superNovaPassive
		var/X, Y, Z
		Used(mob/target, mob/owner, blast/controller)
			var/list/hits = list()
			spawn()
				while(controller)
					hits.Cut()
					X = controller.B.Cx()
					Y = controller.B.Cy()
					Z = controller.B.z
					for(var/mob/M in controller.B.area(128))
						if(controller.owner.canHit(M))
							M.healthChange(-0.6, _hitter = controller.owner)
					sleep(world.tick_lag)
				var/skill/S = new /skill/superNovaExplode
				S.Used(X, Y, Z)
	superNovaExplode
		Used(X, Y, Z, dir)
			new /explosion(null, 256, 128, _damage = 40, _icon = 'bigExplosion.dmi', _knockX = 16, _knockY = 16, _X = X, _Y = Y, _Z = Z, _time = 10)