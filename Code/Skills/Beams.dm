beam
	var/obj/beam/trail/T
	var/obj/beam/head/H
	var/obj/beam/body/B
	var/mob/owner
	var/bind/iconState/BIND
	var/icon/icon
	var dir
	var list/proj = list()
	var skill/castSkill // the skill that casted this.
	var moveSpeed
	var damage
	var end
	var/skill/S
	var name
	var/tier
	var/multiHit
	var/list/hits = list()
	var/hitTime
	var/allyHit
	var/bindRemove
	var/dense
	var/wallEnd
	var endTime
	New(mob/_owner, icon/_icon, _bindTime, _moveSpeed, _damage, _skill, _tier = 1, _castSkill, _name, _dense = 1, _multiHit = 0, _allyHit = 1, _bindRemove = 1, _wallEnd = 1, _hitTime = 2, _beamState = "BEAM")
		owner = _owner
		icon = _icon
		moveSpeed = _moveSpeed
		damage = _damage
		BIND = new /bind/iconState(_time = _bindTime + 1, _iconState = _beamState)
		endTime = world.time + _bindTime
		dir = owner.dir
		S = _skill
		name = _name
		tier = _tier
		dense = _dense
		allyHit = _allyHit
		bindRemove = _bindRemove
		wallEnd = _wallEnd
		hitTime = _hitTime
		castSkill = _castSkill
		beamStart()
	proc/beamStart()
		owner.canFall--
		owner.binds.Add(BIND)
		T = new /obj/beam/trail(src, icon, dense)
		T.SetCenter(owner.Cx() + (owner.bound_width * Directions.ToOffsetX(dir)), owner.Cy(), owner.z)
		H = new /obj/beam/head(src, icon, dense)
		H.SetCenter(T.Cx() + (H.bound_width * Directions.ToOffsetX(dir)), T.Cy(), T.z)
		B = new /obj/beam/body(src, icon, dense)
		B.SetCenter(T.Cx() + (T.bound_width * Directions.ToOffsetX(dir)), T.Cy(), T.z)
		proj.Add(T, H, B)
		for(var/obj/beam/B in proj)
			for(var/O in obounds(B))
				bumpCalc(O)
		beamLoop()
	proc/beamLoop()
		var beamPos = H.Cx() + (B.bound_width * Directions.ToOffsetX(dir))
		spawn()
			while((BIND in owner.binds) && endTime > world.time)
				if(!(owner in moveLoop))
					beamEnd()
					return
				if(!dense)
					for(var/obj/beam/B in proj)
						for(var/O in obounds(B))
							bumpCalc(O)
				H.Translate(moveSpeed * Directions.ToOffsetX(dir))
				if(H.Cx() > beamPos + ((H.bound_width / 2) * Directions.ToOffsetX(dir)))
					var/obj/beam/body/body = new /obj/beam/body(src, icon)
					proj.Add(body)
					body.SetCenter(beamPos, T.Cy(), T.z)
					beamPos = H.Cx()
				if(!dense)
					for(var/obj/beam/BEAM in proj)
						for(var/atom/O in obounds(BEAM))
							bumpCalc(O)
				sleep(world.tick_lag)
			beamEnd()
	proc/beamEnd()
		owner.canFall++
		for(var/obj/O in proj)
			del(O)
		owner.binds.Remove(BIND)
		del(src)
	proc/bumpCalc(atom/O)
		if(O in proj)
			return
		else if(isturf(O) && O.density && wallEnd)
			beamEnd()
		if(ismob(O))
			var/mob/M = O
			if(!multiHit && (M.charName in hits) || hits[M.charName] > world.time || M.dead)
				return
			if(M == owner || owner.canHit(M) || allyHit)
				hits[M.charName] = world.time + hitTime
				M.healthChange(-damage, _hitter = src, _bindRemove = bindRemove)
				if(S)
					S.Used(M, owner, src)
				if(dense)
					beamEnd()
		if(istype(O, /obj/beam))
			var/obj/beam/B = O
			if(B.controller.tier > tier)
				beamEnd()
			else if(B.controller.tier == tier)
				B.controller.beamEnd()
				beamEnd()
			else if(B.controller.tier < tier)
				B.controller.beamEnd()
		if(istype(O, /obj/blast))
			var/obj/blast/B = O
			if(B.controller.tier > tier)
				beamEnd()
			else if(B.controller.tier == tier)
				B.controller.projectileEnd()
				beamEnd()
			else if(B.controller.tier < tier)
				B.controller.projectileEnd()
obj
	beam
		layer = MOB_LAYER+2
		pixel_y = -16
		var beam/controller
		Bump(atom/O)
			controller.bumpCalc(O)
		Cross(atom/O)
			controller.bumpCalc(O)
		Crossed(atom/O)
			if(O in controller.proj)
				return TRUE
			. = ..()
		New(beam/_controller, _icon, _density)
			controller = _controller
			dir = controller.dir
			icon = _icon
			density = _density
		trail
			icon_state = "TRAIL"
		head
			icon_state = "HEAD"
		body
			layer = MOB_LAYER+1
			icon_state = "BODY"