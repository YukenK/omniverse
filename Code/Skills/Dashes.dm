dash
	var moveSpeed
	var mob/owner
	var skill/skill
	var time
	var iconState
	var damage
	var stun
	var list/hits = list()
	New(mob/_owner, _moveSpeed, skill/_skill, _time, _iconState, _damage, _stun)
		owner = _owner
		moveSpeed = _moveSpeed
		skill = _skill
		time = _time
		iconState = _iconState
		damage = _damage
		stun = _stun
		dashLoop()
	proc/dashLoop()
		var/bind/B = new /bind/iconState(_time = time, _iconState = iconState)
		owner.binds.Add(B)
		owner.canFall = 0
		while(B in owner.binds)
			if(!(owner in moveLoop))
				return
			owner.skillVelX = moveSpeed * Directions.ToOffsetX(owner.dir)
			for(var/mob/M in owner.front(16))
				if(!(M in hits))
					hits.Add(M)
					M.healthChange(-damage, _hitter = src)
					if(skill)
						skill.Used(M, owner, src)
					if(stun)
						M.binds.Add(new /bind/iconState(_time = stun, _iconState = "STUN"))
					owner.binds.Remove(B)
				break
			sleep(world.tick_lag)
		dashEnd()
	proc/dashEnd()
		owner.canFall = 1
		del(src)