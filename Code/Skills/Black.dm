skill
	blackKamehameha
		id = "blackkamehameha"
		cooldownTime = 600
		cooldownID = "blackkamehameha"
		energyCost = 100
		castTime = 12
		castState = "CHARGE"
		name = "Black Kamehameha"
		desc = "Quick-fire beam, deals damage over time. Binds & pushes enemies along it."
		Used(mob/user)
			..()
			var/beam/B = new /beam(user, 'superKamehameha.dmi', 20, 12, 14, _castSkill = src, _name = "Black Kamehameha", _dense = 0, _skill = new /skill/superKamehamehaPassive, _wallEnd = 0, _multiHit = 1)
			B.H.pixel_y = -46
			if(B.dir == WEST)
				B.T.pixel_x = -96
	blackBullet
		id = "blackbullet"
		name = "Black Bullet"
		desc = "Quick-fire blast, deals 30 damage & explodes."
		cooldownTime = 200
		energyCost = 40
		skillTime = 3
		skillState = "BLAST"
		cooldownID = "blackbullet"
		Used(mob/user)
			..()
			new /blast(user, _icon = 'blackBullet.dmi', _height = 72, _damage = 30, _time = 20, _moveSpeed = 10, _skill = new /skill/blackBulletPassive, _castSkill = src, _name = "Black Bullet")
	blackBulletPassive
		Used(mob/target, mob/user, blast/controller)
			new /explosion(target, 96, 96, _damage = 20, _icon = 'Explosion.dmi', _knockX = 16, _knockY = 24, _time = 5)
	blackTransmission
		id = "blacktransmission"
		name = "Instant Transmission"
		desc = "If you are hit within the next 2 seconds, teleport behind the enemy, stunning them."
		cooldownID = "blacktransmission"
		cooldownTime = 120
		energyCost = 20
		Used(mob/user)
			..()
			var/skill/S = new /skill/instantTransmissionPassive
			user.hitPassives.Add(S)
			var/time = world.time + 20
			while(time > world.time)
				if(!(user in moveLoop))
					return
				sleep(world.tick_lag)
			user.hitPassives.Remove(S)
	instantTransmissionPassive
		Used(mob/user, target)
			flick("TELE", user)
			var/mob/M = getType(target)
			user.dir = M.dir
			user.SetCenter(M.Cx() + (M.bound_width * -Directions.ToOffsetX(user.dir)), M.Cy(), M.z)
	timeDistortion
		id = "timedistortion"
		name = "Time Ring: Distortion"
		desc = "Heal yourself & all nearby allies."
		cooldownID = "timedistortion"
		cooldownTime = 200
		energyCost = 60
		skillTime = 5
		skillState = "RANGE"
		Used(mob/user)
			..()
			user.healthChange(50)
			for(var/mob/M in user.area(128))
				if(M.team == user.team && !M.dead)
					M.healthChange(50)
	godSlicer
		id = "godslicer"
		name = "God Slicer"
		desc = "Dash forward, dealing damage & inflicting bleed on the first player you hit."
		cooldownID = "godslicer"
		cooldownTime = 300
		energyCost = 60
		Used(mob/user)
			..()
			new /dash(user, 20, new /skill/godSlicerPassive, _time = 12, _iconState = "RUSH", _stun = 2)
	godSlicerPassive
		Used(mob/target, mob/user, dash/controller)
			var/time = world.time + 4
			var/bind/B = new /bind(_time = 5)
			while(time > world.time)
				if(!(B in (user.binds || target.binds)) || !(user in moveLoop) || !(target in moveLoop))
					return
				sleep(world.tick_lag)
			target.effects.Add(new /effect/bleed(_time = 30))
