skill
	chaosBeam
		id = "chaosbeam"
		name = "Death Beam"
		desc = "Quick-fire beam, deals 45 damage."
		cooldownTime = 200
		energyCost = 50
		cooldownID = "deathbeam"
		Used(mob/user)
			..()
			new /beam(user, _icon = 'deathBeam.dmi', _damage = 45, _bindTime = 20, _moveSpeed = 15, _castSkill = src, _name = "Chaos Beam", _tier = 1, _skill = /skill/chaosBeamPassive)
	chaosBeamPassive
		Used(mob/target, mob/user, controller/beam)
			new /explosion(target, 96, 96, _damage = 20, _icon = 'Explosion.dmi', _knockX = 16, _knockY = 24, _time = 5)
	deathCannon
		id = "deathcannon"
		name = "Death Cannon"
		desc = "Quick-fire beam, deals 50 damage."
		cooldownTime = 200
		cooldownID = "deathcannon"
		energyCost = 60
		Used(mob/user)
			..()
			var/beam/B = new /beam(user, _icon = 'deathWave.dmi', _damage = 50, _bindTime = 20, _moveSpeed = 15, _castSkill = src, _name = "Death Cannon", _tier = 1)
			if(B && user.dir == WEST)
				B.T.pixel_x = -32
	phantomFist
		id = "phantomfist"
		cooldownID = "phantomfist"
		name = "Phantom Fist"
		desc = "Damage, stun & knockback any enemies in melee range."
		cooldownTime = 60
		energyCost = 30
		skillTime = 12
		skillState = "COMBO2"
		Used(mob/user)
			..()
			for(var/mob/M in user.front(48))
				if(user.canHit(M))
					M.healthChange(-18, _hitter = user)
					M.binds.Add(new /bind/iconState(_time = 15, _iconState = "STUN"))
					M.skillVelX += 16 * -Directions.ToOffsetX(M.dir)
	assaultDeathBullet
		id = "assaultdeathbullet"
		name = "Assault: Death Bullet"
		desc = "Rapidly fire blasts for 1.2 seconds, once every 0.2 seconds."
		cooldownTime = 150
		energyCost = 40
		cooldownID = "assaultdeathbullet"
		Used(mob/user)
			..()
			var/bind/B = new /bind/iconState(_time = 13, _iconState = "COMBO")
			var/time
			user.binds.Add(B)
			while(B in user.binds)
				if(!(user in moveLoop))
					return
				if(world.time > time)
					time = world.time + 2
					spawn()
						new /blast(user, _icon = 'deathBullet.dmi', _damage = 9, _time = 15, _moveSpeed = 12, _castSkill = src, _name = "Death Bullet")
				sleep(world.tick_lag)
	chaosBall
		id = "chaosball"
		name = "Chaos Ball"
		desc = "Quick-fire blast, deals 30 damage & explodes."
		cooldownTime = 200
		energyCost = 40
		cooldownID = "chaosball"
		Used(mob/user)
			..()
			new /blast(user, _icon = 'chaosBall.dmi', _damage = 30, _time = 20, _moveSpeed = 10, _skill = new /skill/chaosBallPassive, _castSkill = src, _name = "Chaos Ball")
	chaosBallPassive
		Used(mob/target, mob/user, blast/controller)
			new /explosion(target, 96, 96, _damage = 20, _icon = 'Explosion.dmi', _knockX = 16, _knockY = 24, _time = 5)