skill
	masenko
		id = "masenko"
		cooldownID = "masenko"
		cooldownTime = 200
		chargeState = "CHARGE"
		chargeTime = 3
		energyCost = 40
		name = "Masenko"
		desc = "Quick-fire beam, deals 55 damage."
		Used(mob/user)
			..()
			new /beam(user, 'Masenko.dmi', 20, 12, 55, _castSkill = src, _name = "Masenko", _beamState = "BEAM2")
	heal
		id = "heal"
		cooldownID = "heal"
		name = "Heal"
		desc = "Heal all nearby allies, including yourself, for 50 health."
		cooldownTime = 200
		energyCost = 60
		skillTime = 5
		skillState = "HEAL"
		skillState = "RANGE"
		Used(mob/user)
			..()
			user.healthChange(50)
			for(var/mob/M in user.area(128))
				if(M.team == user.team && !M.dead)
					M.healthChange(50)
	evilContainmentWave
		id = "evilconwave"
		cooldownID = "evilconwave"
		cooldownTime = 600
		energyCost = 80
		name = "Evil Containment Wave"
		desc = "Quick-fire beam, deals 0 damage. If the target is at or below 35% of their max health, instantly kill & seal them."
		chargeState = "CHARGE3"
		chargeTime = 6
		Used(mob/user)
			..()
			new /beam(user, 'Mafuba.dmi', 20, 12, 0, _skill = new /skill/evilContainmentWavePassive, _castSkill = src, _name = "Evil Containment Wave", _beamState = "BEAM3")
	evilContainmentWavePassive
		Used(mob/target, mob/user, beam/controller)
			if(target.health <= target.maxHealth * 0.35)
				target.health = -target.maxHealth
				target.deathCheck(controller)
	godBreaker
		id = "godbreaker"
		cooldownID = "godbreaker"
		cooldownTime = 100
		energyCost = 60
		name = "God Breaker"
		desc = "Fly up into the air - after 0.5 seconds, send a beam out that damages all nearby enemies."
		skillTime = 10
		skillState = "BEAM3"
		Used(mob/user)
			..()
			var/bind/B = new /bind(_time = 12)
			user.binds.Add(B)
			var/time = world.time + 5
			user.canFall--
			while(time > world.time)
				if(!(user in moveLoop) || !(B in user.binds))
					return
				user.Translate(0, 2)
				sleep(world.tick_lag)
			if(B in user.binds)
				user.oldIcon = user.icon
				user.icon = 'trunksGodBreaker.dmi'
				time = world.time + 5
				for(var/mob/M in user.area(196))
					user.healthChange(-40, _hitter = src)
			while(time > world.time)
				if(!(user in moveLoop) || !(B in user.binds))
					return
				sleep(world.tick_lag)
			user.icon = user.oldIcon
			user.canFall++