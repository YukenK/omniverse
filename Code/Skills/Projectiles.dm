blast
	var/obj/blast/B
	var/bind/BIND
	var/mob/owner
	var/skill/S
	var/icon/icon
	var/time
	var/width = 32
	var/height = 32
	var/dir
	var/moveSpeed
	var/damage
	var name
	var/tier = 0.9
	var skill/castSkill // the skill that casted this.
	var skillUse // If the skill is used as soon as the blast starts.
	var dense = 1
	var/multiHit = 0 // If an undense projectile will hit someone multiple times.
	var/list/hits = list()
	var allyHit = 1
	var castLoop
	var homing
	var/mob/target
	New(mob/_owner, _width = 32, _height = 32, _icon, _damage, _skill, _time, _moveSpeed, _BIND, _tier = 0.9, _skillUse, _allyHit = 1, _castSkill, _name, _dense = 1, _multiHit = 0, _castLoop = 0, _homing, _target)
		S = _skill
		owner = _owner
		icon = _icon
		damage = _damage
		moveSpeed = _moveSpeed
		time = world.time + _time
		dir = owner.dir
		tier = _tier
		width = _width
		height = _height
		dense = _dense
		multiHit = _multiHit
		allyHit = _allyHit
		BIND = _BIND
		skillUse = _skillUse
		name = _name
		castSkill = _castSkill
		castLoop = _castLoop
		homing = _homing
		target = _target
		projectileStart()
	proc/projectileStart()
		if(BIND)
			owner.binds.Add(BIND)
		B = new /obj/blast(src, height, width, dense)
		B.SetCenter(owner.Cx() + (owner.bound_width * Directions.ToOffsetX(dir)) + (B.bound_width / 2 * Directions.ToOffsetX(dir)) , owner.Cy() + (B.bound_height - owner.bound_height), owner.z)
		if(skillUse && S)
			S.Used(target = null, owner = owner, controller = src)
		for(var/O in obounds(B))
			bumpCalc(O)
		projectileLoop()
	proc/projectileLoop()
		if(homing)
			walk_to(B, target, 0, 0, moveSpeed)
		while(time > world.time && B)
			if(!(owner in moveLoop))
				projectileEnd()
				return
			if(BIND && !(BIND in owner.binds))
				break
			if(!homing)
				B.Translate(moveSpeed * Directions.ToOffsetX(dir))
			if(castLoop)
				S.Used(src)
			if(!dense)
				for(var/atom/O in obounds(B))
					bumpCalc(O)
			sleep(world.tick_lag)
		projectileEnd()
	proc/projectileEnd()
		del(B)
		del(src)
	proc/bumpCalc(atom/O)
		if(ismob(O))
			var/mob/M = O
			if((M in hits) && !multiHit)
				return
			if(M == owner || owner.canHit(M) || allyHit && !M.dead)
				hits.Add(M)
				M.healthChange(-damage, _hitter = src)
				if(S && !skillUse)
					S.Used(target = M, owner = owner, controller = src)
				if(dense)
					projectileEnd()
		if(isturf(O) && O.density)
			projectileEnd()
			return
		if(istype(O, /obj/beam))
			var/obj/beam/BEAM = O
			if(BEAM.controller.tier > tier)
				projectileEnd()
			else if(BEAM.controller.tier == tier)
				BEAM.controller.beamEnd()
				projectileEnd()
			else if(BEAM.controller.tier < tier)
				BEAM.controller.beamEnd()
			return
		if(istype(O, /obj/blast))
			var/obj/blast/BLAST = O
			if(BLAST.controller.tier > tier)
				projectileEnd()
			else if(BLAST.controller.tier == tier)
				BLAST.controller.projectileEnd()
				projectileEnd()
			else if(BLAST.controller.tier < tier)
				BLAST.controller.projectileEnd()
			return
obj/blast
	var/blast/controller
	New(blast/_controller, _width, _height, _density)
		controller = _controller
		bound_width = _width
		bound_height = _height
		icon = controller.icon
		dir = controller.dir
		density = _density
	Bump(atom/O)
		controller.bumpCalc(O)
	Cross(atom/O)
		controller.bumpCalc(O)