skill
	powerup
		id = "powerup"
		doesCharge = 1
		minChargeTime = 4
		minChargeState = "POWER"
		chargeState = "STANCE"
		chargeTime = -1
		staminaCost = 0.375
		cooldownID = "powerup"
		chargeID = "powerup"
		doesCD = 0
		endCD = 1
		cooldownTime = 30
		Used(mob/user)
			..()
			user.energyChange(0.375)
			if(user.energy >= user.maxEnergy || user.stamina < 10)
				user.cooldowns[cooldownID] = world.time + cooldownTime