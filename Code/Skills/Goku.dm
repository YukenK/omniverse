skill
	kamehameha
		id = "kamehameha"
		name = "Kamehameha"
		desc = "Quick-fire beam, deals 65 damage."
		castTime = 2
		energyCost = 75
		castState = "CHARGE"
		castFlick = "SKILL1"
		cooldownID = "kamehameha"
		cooldownTime = 150
		Used(mob/user)
			..()
			var/bind/B = new /bind(_time = 3)
			user.binds.Add(B)
			if(B in user.binds)
				new /beam(user, 'Kamehameha.dmi', 20, 12, 65, _castSkill = src, _name = "Kamehameha")
	energyBlast
		id = "energyblast"
		name = "Energy Blast"
		desc = "Quick-fire blast, deals 10 damage & knockbacks."
		skillTime = 1
		skillState = "SKILL2"
		cooldownID = "energyblast"
		energyCost = 20
		cooldownTime = 8
		Used(mob/user)
			..()
			new /blast(user, _icon = 'Blast.dmi', _damage = 10, _skill = new /skill/energyBlastPassive, _time = 15, _moveSpeed = 12, _castSkill = src, _name = "Energy Blast")
	energyBlastPassive
		Used(mob/target, mob/owner, blast/controller)
			target.skillVelX += 16 * Directions.ToOffsetX(controller.dir)
	kaiokenRush
		castTime = 5
		id = "kaiokenrush"
		name = "Kaioken: Rush"
		desc = "Teleport behind all nearby enemies, in 0.4 second delays, dealing 28% of their max health to the first person, then -2% per person. When casted, deal damage equal to 20% of your max health to yourself to a minimum of 10 health. If hit or if the damage is below 20%, this combo ends."
		cooldownID = "kaiokenrush"
		castFlick = "RUSH"
		energyCost = 70
		cooldownTime = 250
		Used(mob/user)
			..()
			var/dir = user.dir
			var/vector/oldLoc = new /vector(user.Cx(), user.Cy(), user.z)
			var/perc = 0.28
			var/bind/B = new /bind(_time = 20)
			user.binds.Add(B)
			user.healthChange(-(user.maxHealth * 0.2), _bindRemove = 0, _minHealth = 10)
			for(var/mob/M in view(user))
				if(user.canHit(M) && (B in user.binds) && perc >= 0.2)
					flick("COMBO", user)
					user.dir = M.dir
					user.SetCenter(M.Cx() + (M.bound_width * -Directions.ToOffsetX(M.dir)), M.Cy(), M.z)
					M.healthChange(-(M.maxHealth * perc), hitter = src)
					perc -= 0.02
					sleep(4)
			user.binds.Remove(B)
			user.dir = dir
			user.SetCenter(oldLoc.x, oldLoc.y, oldLoc.z)
	spiritBomb
		id = "spiritbomb"
		cooldownID = "spiritbomb"
		name = "Spirit Bomb"
		desc = "Hold to charge the Spirit Bomb, for a maximum of 5 seconds, dealing 110 damage if fully charged. If you take damage, the Spirit Bomb will not fire. For every ally nearby when this is fired, it will deal an extra 33% damage to a max of 250."
		chargeTurn = 1
		energyCost = 100
		cooldownTime = 750
		endCD = 1
		doesCharge = 1
		damageEnd = 1
		chargePercentage = 1
		minChargeTime = 10
		chargeTime = 50
		chargeState = "SKILL3"
		minChargeState = "BOMBCHARGE"
		Used(mob/user, charged)
			..()
			new /blast(user, _icon = 'spiritBomb.dmi', _time = 30, _height = 96, _damage = 95 * charged, _moveSpeed = 14, _skillUse = 1, _skill = new /skill/spiritBombPassive, _tier = 3, _castSkill = src, _name = "Spirit Bomb")
	spiritBombPassive
		Used(mob/target, mob/owner, blast/controller)
			for(var/mob/M in view(owner))
				if(M.team == owner.team && !M.dead)
					controller.damage = clamp(controller.damage + (controller.damage * 0.33), 95, 180)
	instantTransmission
		id = "instanttransmission"
		cooldownID = "instanttransmission"
		castTime = 12
		energyCost = 40
		cooldownTime = 1800
		castState = "SKILL4"
		skillTime = 4
		skillRemove = 0
		name = "Instant Transmission"
		desc = "If uninterrupted, teleport back to spawn along with any allies within 2 tiles."
		Used(mob/user)
			..()
			var/bind/B = new /bind(_time = 5)
			var/time = world.time + 4
			user.binds.Add(B)
			flick("SKILL5", user)
			var/list/teleport = list(user)
			for(var/mob/M in user.area(64))
				if(user.canHit(M))
					teleport.Add(M)
			while(B in user.binds)
				if(world.time > time)
					for(var/mob/M in teleport)
						M.mapSet()
					break
				sleep(world.tick_lag)
	superKamehameha
		id = "superkamehameha"
		cooldownTime = 600
		cooldownID = "superkamehameha"
		energyCost = 100
		castTime = 12
		castState = "CHARGE"
		name = "Super Kamehameha"
		desc = "Quick-fire beam, deals damage over time. Binds & pushes enemies along it."
		Used(mob/user)
			..()
			var/beam/B = new /beam(user, 'superKamehameha.dmi', 20, 12, 14, _castSkill = src, _name = "Super Kamehameha", _dense = 0, _skill = new /skill/superKamehamehaPassive, _wallEnd = 0, _multiHit = 1)
			B.H.pixel_y = -46
			if(B.dir == WEST)
				B.T.pixel_x = -96
	superKamehamehaPassive
		var/list/hits = list()
		Used(mob/target, mob/owner, beam/controller)
			if(world.time >= hits[target.charName])
				target.binds.Add(new /bind/iconState(_time = 4, _iconState = "STUN"))
				hits[target.charName] = world.time + 8
			if(controller.dir == EAST && target.skillVelX < controller.moveSpeed)
				target.skillVelX = clamp(target.skillVelX + controller.moveSpeed, -300, controller.moveSpeed)
			else if(controller.dir == WEST && target.skillVelX > -controller.moveSpeed)
				target.skillVelX = clamp(target.skillVelX - controller.moveSpeed, -controller.moveSpeed, 300)