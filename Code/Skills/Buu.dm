skill
	copyCat
		id = "Copycat"
		desc = "When hit by a blast or beam, you gain the ability to cast it yourself."
	copyCatPassive
		Used(mob/user, hitter)
			if(istype(hitter, /beam))
				var/beam/B = hitter
				if(B.castSkill && B.tier <= 2.5)
					user.hotbar[1] = new B.castSkill
			if(istype(hitter, /blast))
				var/blast/B = hitter
				if(B.castSkill && B.tier <= 2.5)
					user.hotbar[1] = new B.castSkill