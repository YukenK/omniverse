skill
	bloodySmash
		id = "bloodysmash"
		name = "Bloody Smash"
		desc = "Target the first enemy within 3 tiles, knocking them backwards, stomping them into the ground and then kicking them away."
		cooldownID = "bloodysmash"
		energyCost = 50
		cooldownTime = 300
		Used(mob/user)
			..()
			for(var/mob/M in user.front(96))
				if(user.canHit(M))
					var/bind/B = new /bind(_binders = list(user, M), _time = 19)
					user.binds.Add(B)
					var/time = world.time - 1
					var/part = 0
					var/vector/oldLoc = new /vector(user.Cx(), user.Cy(), user.z)
					while(B in user.binds)
						if(world.time > time)
							switch(part)
								if(0)
									M.healthChange(-15, _hitter = user, _bindRemove = 0)
									time = world.time + 5
									M.skillVelX += 16 * Directions.ToOffsetX(user.dir)
									M.binds.Add(new /bind/iconState(_iconState = "STUN", _time = 5))
									part++
								if(1)
									user.SetCenter(M.Cx(), M.Cy() + M.bound_height, M.z)
									M.healthChange(-30)
									M.binds.Add(new /bind/iconState(_iconState = "STUN", _time = 5))
									time = world.time + 9
									part++
								if(2)
									user.SetCenter(M.Cx() + (M.bound_width / 2) * -Directions.ToOffsetX(user.dir), M.Cy(), M.z)
									M.healthChange(-15)
									M.binds.Add(new /bind/iconState(_iconState = "STUN", _time = 5))
						sleep(world.tick_lag)
					user.SetCenter(oldLoc.x, oldLoc.y, oldLoc.z)
					break
	eraserBlow
		id = "eraserblow"
		name = "Eraser Blow"
		desc = "Quick-fire blast, deals 30 damage & knockbacks."
		skillTime = 3
		skillState = "BLAST"
		cooldownID = "energyblast"
		energyCost = 20
		cooldownTime = 100
		Used(mob/user)
			..()
			var/target
			var/homing
			for(var/mob/M in view(user))
				if(user.canHit(M) && user.frontDir(M))
					target = M
					homing++
					break
			new /blast(user, _icon = 'eraserBlow.dmi', _damage = 30, _time = 15, _moveSpeed = 12, _castSkill = src, _name = "Eraser Blow", _homing = homing, _target = target)
	eraserShot
		id = "erasershot"
		name = "Eraser Shot"
		desc = "Quick-fire blast, deals 30 damage, passes through enemies & knockbacks."
		castTime = 4
		castState = "CHARGE"
		skillTime = 5
		skillState = "BLAST2"
		cooldownID = "erasershot"
		energyCost = 40
		cooldownTime = 100
		Used(mob/user)
			..()
			new /blast(user, _icon = 'eraserShot.dmi', _damage = 30, _skill = new /skill/eraserShotPassive, _moveSpeed = 12, _time = 40, _castSkill = src, _name = "Eraser Shot", _tier = 2)
	blasterMeteor
		id = "blastermeteor"
		name = "Blaster Meteor"
		desc = "Quickly fire several blasts in all directions, dealing 9 damage each."
		skillTime = 18
		skillState = "RANGE"
		cooldownID = "blastermeteor"
		energyCost = 40
		cooldownTime = 300
		Used(mob/user)
			..()
			var/bind/B = new /bind(_time = 18)
			user.binds.Add(B)
			var/time = world.time - 1
			while(B in user.binds)
				if(world.time > time)
					user.dir = pick(EAST, WEST)
					spawn()
						new /blast(user, _icon = 'Blast.dmi', _damage = 9, _skill = new /skill/energyBlastPassive, _time = 15, _moveSpeed = 12, _castSkill = src, _name = "Blaster Meteor")
					time = world.time + 2
				sleep(world.tick_lag)
	eraserCannon
		id = "erasercannon"
		name = "Eraser Cannon"
		desc = "Quick-fire blast, deals 20 damage - for every enemy it passes through, it doubles in damage (up to 80)."
		skillTime = 8
		skillState = "BLAST3"
		cooldownID = "erasercannon"
		energyCost = 70
		cooldownTime = 450
		Used(mob/user)
			..()
			new /blast(user, _icon = 'eraserCannon.dmi', _damage = 15, _skill = new /skill/eraserCannonPassive, _moveSpeed = 12, _time = 40, _castSkill = src, _name = "Eraser Cannon", _tier = 3)
	eraserCannonPassive
		Used(mob/target, mob/user, blast/controller)
			controller.damage = clamp(controller.damage * 2, 20, 80)
			controller.B.icon = 'eraserCannonBig.dmi'
	eraserShotPassive
		Used(mob/target)
			target.skillVelY += 16
			target.binds.Add(new /bind/iconState(_iconState = "STUN", _time = 10))