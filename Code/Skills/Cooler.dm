skill
	coolerpower
		id = "coolerpower"
		doesCharge = 1
		minChargeTime = 4
		minChargeState = "POWER"
		chargeState = "STANCE"
		chargeTime = -1
		staminaCost = 0.375
		cooldownID = "powerup"
		chargeID = "powerup"
		doesCD = 0
		endCD = 1
		cooldownTime = 30
		Used(mob/user)
			..()
			user.energyChange(0.28 * clamp((user.maxHealth % user.health), 0.22, 0.99))
			if(user.energy >= user.maxEnergy || user.stamina < 10)
				user.cooldowns[cooldownID] = world.time + cooldownTime
	darknessBeam
		id = "darknessbeam"
		cooldownID = "darknessbeam"
		skillTime = 4
		skillState = "BLAST"
		energyCost = 35
		name = "Darkness Eye Beam"
		desc = "Quick-fire blast, deals 30 damage."
		cooldownTime = 200
		Used(mob/user)
			..()
			new /blast(user, _icon = 'coolerLaser.dmi', _moveSpeed = 14, _damage = 40, _tier = 2, _castSkill = src, _name = "Darkness Beam", _time = 25)
	superDeathBeam
		id = "superdeathbeam"
		name = "Super Death Beam"
		skillTime = 2
		skillState = "SKILL2"
		desc = "Quick-fire beam, deals 55 damage, passes through enemies."
		cooldownTime = 250
		energyCost = 50
		cooldownID = "superdeathbeam"
		Used(mob/user)
			..()
			new /beam(user, _icon = 'deathBeam.dmi', _damage = 40, _bindTime = 20, _dense = 0, _moveSpeed = 14, _castSkill = src, _name = "Super Death Beam", _tier = 2)
	sauzerBlade
		id = "sauzerblade"
		name = "Sauzer Blade"
		desc = "Dash forward, dealing damage with a 40% chance to inflict bleed on the first player you hit."
		cooldownID = "sauzerblade"
		cooldownTime = 300
		energyCost = 60
		Used(mob/user)
			..()
			new /dash(user, 30, new /skill/sauzerBladePassive, _time = 14, _iconState = "RUSH", _stun = 2)
	sauzerBladePassive
		Used(mob/target, mob/user, dash/controller)
			if(prob(40))
				target.effects.Add(new /effect/bleed(_time = 30))
	psychoBarrier
		id = "psychobarrier"
		name = "Psycho Barrier"
		desc = "Block all damage for two seconds, increasing your health regeneration the lower your health is."
		cooldownID = "psychobarrier"
		cooldownTime = 400
		energyCost = 40
		Used(mob/user)
			..()
			var/bind/B = new /bind/iconState(_time = 20, _iconState = "SHIELD", _damageRemove = 0)
			user.binds.Add(B)
			var/effect/shield/S = new /effect/shield(_time = 20)
			user.effects.Add(S)
			var/buff/BF
			var/hR = (user.baseHR * (user.maxHealth % user.health)) - user.baseHR
			for(var/buff/BUFF in user.buffs)
				if(BUFF.id == "default")
					BF = BUFF
					break
			BF.hR += hR
			while((B in user.binds) && (S in user.effects))
				if(!(user in moveLoop))
					return
				sleep(world.tick_lag)
			BF.hR -= hR
	coolerSuperNova
		id = "coolersupernova"
		name = "Supernova"
		desc = "If uninterrupted for 0.3 seconds, summon a Supernova. Deals AOE damage and explodes."
		castTime = 3
		castState = "CHARGE"
		skillTime = 4
		skillState = "BOMB"
		energyCost = 100
		cooldownTime = 600
		cooldownID = "supernova"
		chargeTurn = 1
		Used(mob/user)
			..()
			new /blast(user, _icon = 'Supernova.dmi', _height = 96, _skill = new /skill/superNovaPassive, _time = 40, _moveSpeed = 10, _skillUse = 1, _castSkill = src, _name = "Supernova", _tier = 3)
