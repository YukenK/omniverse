var map = "City"
var roundEnding
proc
	canRoundEnd()
		if(zfighters && !soldiers && !warriors || soldiers && !warriors && !zfighters || warriors && !soldiers && !zfighters || !warriors && !soldiers && !zfighters)
			return TRUE
		return FALSE
	roundEnd()
		if(canRoundEnd() && !roundEnding)
			var roundWinner
			var roundFont
			var roundEnd
			if(zfighters && !soldiers && !warriors)
				roundWinner = "Z-Fighters"
				roundFont = "cyan"
			if(soldiers && !warriors && !zfighters)
				roundWinner = "Soldiers"
				roundFont = "purple"
			if(warriors && !soldiers && !zfighters)
				roundWinner = "Warriors"
				roundFont = "red"
			if(!warriors && !soldiers && !zfighters)
				roundWinner = "Admins"
				roundFont = "yellow"
			roundEnd = world.time + 80
			world << output("<font size = 4 color = [roundFont]>The [roundWinner] have won the round!", "feed")
			roundEnding = 1
			while(canRoundEnd())
				if(world.time >= roundEnd)
					newRound()
					return
				sleep(world.tick_lag)
			roundEnding = 0
			world << output("<font size = 4 color = green>Somebody has come back to life, the round continues!", "feed")
	newRound()
		moveLoop = list()
		zfighters = 0
		soldiers = 0
		warriors = 0
		roundEnding = 0
		for(var/mob/M in world)
			if(M.client)
				M.dead = 0
				M.icon = null
				M.loc = locate(22, 8, 1)
				M.binds = list()
				M.effects = list()
				M.cooldowns = list()
				M.deathPassives = list()
				M.passives = list()
				M.buffs = list()
				M.hotbar = list()
				M.loopPassives = list()
				M.hitPassives = list()
				M.hitOtherPassive = list()
				M.canFall = 1
				M.killPassives = list()
				winshow(M, "health", 0)
				winshow(M, "energy", 0)
				winshow(M, "stamina", 0)
			else
				del(M)
		for(var/beam/B in world)
			B.beamEnd()
		for(var/blast/B in world)
			B.projectileEnd()
		for(var/explosion/E in world)
			E.explosionEnd()
		for(var/obj/character/C in world)
			C.selected = 0
			C.overlays = list()