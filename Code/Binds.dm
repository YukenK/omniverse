bind
	var time
	var damageRemove = 1 // If damage removes the bind. Default of yes.
	var list/binders = list() // Players effected by the bind - this is used for if one player gets hit, the rest should get unbound, in things like bind combos.
	var id
	var turn
	New(_time, _damageRemove, _binders, _id, _turn = 0)
		if(_time == -1)
			time = -1
		else if(_time)
			time = world.time + _time
		if(_damageRemove == 0)
			damageRemove = 0
		if(_id)
			id = _id
		binders = _binders
		turn = _turn
	iconState // Used for binds that should change your icon state.
		var iconState
		New(_time, _damageRemove, _iconState, _id, _turn)
			..()
			iconState = _iconState
mob
	proc
		bindRemove()
			for(var/bind/B in binds)
				if(world.time >= B.time && B.time != -1)
					binds.Remove(B)
				if(B.binders)
					for(var/mob/M in B.binders)
						if(!(B in binds))
							M.binds.Remove(B)