mob
	var nextKnock, nextTrip
	proc
		deathCheck(hitter)
			if(health <= 0)
				if(hitter)
					var/killName
					if(istype(hitter, /beam))
						var/beam/B = hitter
						killName = "[B.owner.charName]'s [B.name]"
					else if(istype(hitter, /blast))
						var/blast/B = hitter
						killName = "[B.owner.charName]'s [B.name]"
					else if(istype(hitter, /mob))
						var/mob/M = hitter
						killName = "[M.charName]"
					world << output("<font color = red>[charName] has been defeated by [killName].", "feed")
				else
					world << output("<font color = red>[charName] has died.", "feed")
				dead = 1
				switch(team)
					if("zfighters")
						zfighters--
					if("soldiers")
						soldiers--
					if("warriors")
						warriors--
				for(var/skill/S in deathPassives)
					S.Used(src, hitter)
				if(hitter && istype(hitter, /mob))
					var/mob/M = hitter
					for(var/skill/S in M.killPassives)
						S.Used(M, src)
				spawn()
					roundEnd()
		canAttack()
			if(stamina < 5)
				return FALSE
			return canMove()
		canHit(mob/M)
			if(team == M.team || M.dead || !M.density)
				return FALSE
			return TRUE
		Attack()
			if(canAttack())
				staminaChange(-5)
				var doesTrip, doesKnock
				var/bind/iconState/B = new /bind(_time = attackDelay, _damageRemove = 0)
				if(trip && world.time >= nextTrip)
					doesTrip = 1
					nextTrip = world.time + 600
					flick("ATTACK4", src)
				else if(knock && world.time >= nextKnock)
					doesKnock = 1
					nextKnock = world.time + 600
					flick("ATTACK3", src)
				else if(!onGround)
					flick("JUMPATTACK", src)
				else
					flick(pick("ATTACK1", "ATTACK2"), src)
				binds.Add(B)
				for(var/mob/M in front(attackRange))
					if(canHit(M))
						M.healthChange(-attack, _hitter = src)
					if(doesKnock)
						M.skillVelX += knockAmount * Directions.ToOffsetX(dir)
						M.skillVelY += knockAmount
						M.binds.Add(new /bind/iconState(_time = 5, _damageRemove = 0, _iconState = "STUN"))
					if(doesTrip)
						M.binds.Add(new /bind/iconState(_time = 3, _damageRemove = 0, _iconState = "STUN"))

